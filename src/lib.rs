pub struct Customer {
    _name: String,
    _rentals: Vec<Rental>,
}

pub const REGULAR: i32 = 0;
pub const NEW_RELEASE: i32 = 1;
pub const CHILDRENS: i32 = 2;

impl Customer {
    pub fn new(name: String) -> Customer {
        return Customer { _name: name, _rentals: Vec::new() };
    }

    pub(crate) fn addRental(&self, rental: Rental) -> Customer {
        let mut rentals = Vec::from(self._rentals.clone());
        rentals.push(rental);
        return Customer {
            _name: self._name.clone(),
            _rentals: rentals,
        };
    }

    pub fn statement(&self) -> String {
        let mut totalAmount = 0.0;
        let mut frequentRenterPoints = 0;
        let mut result = format!("Rental Record for {}\n", self.getName());

        for each in self._rentals.clone() {
            let mut thisAmount = 0.0;

            //determine amounts for each line
            match each.getMovie().getPriceCode() {
                REGULAR => {
                    thisAmount += 2.0;
                    if each.getDaysRented() > 2 {
                        thisAmount += f64::from(each.getDaysRented() - 2) * 1.5
                    }
                }
                NEW_RELEASE => thisAmount += f64::from(each.getDaysRented() * 3),
                CHILDRENS => {
                    thisAmount += 1.5;
                    if each.getDaysRented() > 3 {
                        thisAmount += f64::from(each.getDaysRented() - 3) * 1.5
                    }
                }
                _ => {}
            }

            // add frequent renter points
            frequentRenterPoints += 1;
            // add bonus for a two day new release rental
            if (each.getMovie().getPriceCode() == NEW_RELEASE) && each.getDaysRented() > 1 {
                frequentRenterPoints += 1
            }

            // show figures for this rental
            result.push_str(format!("\t{}\t{}\n", each.getMovie().getTitle(), thisAmount).as_str());
            totalAmount += thisAmount
        }

        // add footer lines
        result.push_str(format!("Amount owed is {}\n", totalAmount).as_str());
        result.push_str(format!("You earned {} frequent renter points", frequentRenterPoints).as_str());

        return result;
    }

    fn getName(&self) -> String {
        return self._name.clone();
    }
}

#[derive(Clone)]
pub struct Rental {
    _movie: Movie,
    _daysRented: i32,
}

impl Rental {
    pub(crate) fn getDaysRented(&self) -> i32 {
        return self._daysRented;
    }

    pub(crate) fn getMovie(&self) -> Movie {
        return self._movie.clone();
    }

    pub(crate) fn new(movie: Movie, dayRented: i32) -> Rental {
        return Rental { _movie: movie, _daysRented: dayRented };
    }
}

#[derive(Clone)]
pub struct Movie {
    _title: String,
    _priceCode: i32,
}

impl Movie {
    pub(crate) fn getTitle(&self) -> &str {
        return self._title.as_str();
    }

    pub(crate) fn getPriceCode(&self) -> i32 {
        return self._priceCode;
    }

    pub(crate) fn new(name: String, priceCode: i32) -> Movie {
        return Movie { _title: name, _priceCode: priceCode };
    }
}

#[cfg(test)]
mod tests_customers {
    use crate::{CHILDRENS, Customer, Movie, NEW_RELEASE, REGULAR, Rental};

    #[test]
    fn test_customer() {
        let customer = Customer::new(String::from("Bob"))
            .addRental(Rental::new(Movie::new(String::from("Jaws"), REGULAR), 2))
            .addRental(Rental::new(Movie::new(String::from("Golden Eye"), REGULAR), 3))
            .addRental(Rental::new(Movie::new(String::from("Short New"), NEW_RELEASE), 1))
            .addRental(Rental::new(Movie::new(String::from("Long New"), NEW_RELEASE), 2))
            .addRental(Rental::new(Movie::new(String::from("Bambi"), CHILDRENS), 3))
            .addRental(Rental::new(Movie::new(String::from("Toy Story"), CHILDRENS), 4));
        let expected = concat!("",
        "Rental Record for Bob\n",
        "\tJaws\t2\n",
        "\tGolden Eye\t3.5\n",
        "\tShort New\t3\n",
        "\tLong New\t6\n",
        "\tBambi\t1.5\n",
        "\tToy Story\t3\n",
        "Amount owed is 19\n",
        "You earned 7 frequent renter points");
        assert_eq!(expected, customer.statement());
    }
}